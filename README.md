# README
Here is a cmake project template for a Qt5 embedded app. To cross-compile the project, provide or modify bbb-toolchain.cmake with your Qt cross-compiler kit.

```

mkdir -p build && cd ./build

cmake ../ -DCMAKE_TOOLCHAIN_FILE=../bbb-toolchain.cmake -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON

make

```

