import QtQuick 2.3

Rectangle {
    id: page
    width: 320; height: 480
    color: "lightgray"

    Text {
        id: helloText
        text: "Hello world!"
        y: 30
        anchors.horizontalCenter: page.horizontalCenter
        font.pointSize: 24; font.bold: true
    }
    Image {
        id: update
        anchors.top :helloText.bottom 
        anchors.horizontalCenter: page.horizontalCenter
        source: "qrc:///images/update.svg"
    }
}

