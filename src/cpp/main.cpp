#include <QApplication>
#include <QGuiApplication>
#include <QtQuick>

int main(int argc, char *argv[]) {

  /**
   * @brief QGuiApplication contains the main event loop, where all events
   * from the window system and other sources are processed and dispatched.
   */
  QApplication app(argc, argv);

  /**
   * @brief The QQuickView class provides a window for displaying a Qt
   * QuickmanageTimer
   * user interface (QML Scene) with the URL of the main source file
   */
  QQuickView view;
  // view.setSource(QUrl("/home/jim/Lab/qt-cmake-project/src/qml/main.qml"));
  view.setSource(QStringLiteral("qrc:///main.qml"));
  view.show();
  return app.exec();
}
